package br.ucsal.edu.bes20182.ed;

import java.util.Scanner;

public class Menu {
	static Scanner sc = new Scanner(System.in);
	public static int limite = 0;

	public static void inputVetor() {
		System.out.println("Informe o n�mero total de funcion�rios: ");
		limite = sc.nextInt();

		for (int i = 0; i < limite; i++) {
			Funcionario.todos[i] = new Funcionario();
			input(i);

		}

	}

	private static void input(int i) {
		String nome;
		Long cpf;
		System.out.println("Informe o cpf: ");
		cpf = sc.nextLong();
		if (contemCpf(cpf) == false) {
			Funcionario.todos[i].cpf = cpf;
			System.out.println("Informe o Nome: ");
			nome = sc.next();
			if (contemNome(nome) == false) {
				Funcionario.todos[i].nome = nome;
				System.out.println("Informe a profiss�o: ");
				Funcionario.todos[i].profissao = sc.next();
				TreeByName.inserir(i);
				TreeByCpf.inserir(i);
			} else {
				while (contemNome(nome) == true) {
					System.out.println("Nome J� Cadastrado!");
					System.out.println("Informe o Nome: ");
					nome = sc.next();
				}
				Funcionario.todos[i].nome = nome;
				System.out.println("Informe a profiss�o: ");
				Funcionario.todos[i].profissao = sc.next();
				TreeByName.inserir(i);
				TreeByCpf.inserir(i);
			}

		} else {
			while (contemCpf(cpf) == true) {
				System.out.println("CPF J� Cadastrado!");
				System.out.println("Informe o CPF: ");
				cpf = sc.nextLong();
			}

			Funcionario.todos[i].cpf = cpf;
			inputByName(i);
		}
	}

	private static void inputByName(int i) {
		String nome;
		System.out.println("Informe o nome: ");
		nome = sc.next();
		if (contemNome(nome) == false) {
			Funcionario.todos[i].nome = nome;
			System.out.println("Informe a profiss�o: ");
			Funcionario.todos[i].profissao = sc.next();
			TreeByName.inserir(i);
			TreeByCpf.inserir(i);
		} else {

			while (contemNome(nome) == true) {
				System.out.println("Nome J� Cadastrado!");
				System.out.println("Informe o Nome: ");
				nome = sc.next();
			}
			Funcionario.todos[i].nome = nome;
			System.out.println("Informe a profiss�o: ");
			Funcionario.todos[i].profissao = sc.next();
			TreeByName.inserir(i);
			TreeByCpf.inserir(i);
		}
	}

	public static void principal() {
		int esc = 0;
		Long cpf;
		String nome;
		System.out.println(
				"Escolha uma op��o para come�ar:\n (1)Busca por CPF\n (2)Imprimir por Cpf \n (3)Remover por Cpf \n "
						+ "(4)Imprimir por Nome \n (5)Buscar por Nome \n (6)Remover por Nome");
		esc = sc.nextInt();
		switch (esc) {
		case 1:
			System.out.println("Informe o numero de cpf a ser buscado: ");
			cpf = sc.nextLong();
			TreeByCpf.buscar(cpf);
			Menu.principal();
			break;
		case 2:
			No auxCpf = TreeByCpf.raizCpf;
			System.out.println("InOrder");
			TreeByCpf.porCpf.inOrder(auxCpf);
			System.out.println();
			Menu.principal();
			break;
		case 3:
			System.out.println("Informe o numero de cpf a ser removido: ");
			cpf = sc.nextLong();
			TreeByCpf.porCpf.remover(cpf);
			Menu.principal();
			break;
		case 4:
			No auxNome = TreeByName.raizNome;
			System.out.println("InOrder");
			TreeByCpf.porCpf.inOrder(auxNome);
			System.out.println();
			Menu.principal();
			break;
		case 5:
			System.out.println("Informe o nome a ser buscado: ");
			nome = sc.next();
			TreeByName.buscar(nome);
			Menu.principal();
			break;
		case 6:
			System.out.println("Informe o nome a ser removido: ");
			nome = sc.next();
			TreeByName.porNome.remover(nome);
			Menu.principal();
			break;
		default:
			break;
		}
	}

	public static boolean contemNome(String nome) {
		if (TreeByName.raizNome == null) {
			return false;
		} else {
			No aux = TreeByName.raizNome;
			while (!Funcionario.todos[aux.indice].nome.equalsIgnoreCase(nome)) {

				if (nome.compareToIgnoreCase(Funcionario.todos[aux.indice].nome) < 0) {
					aux = aux.esq;
				} else {
					aux = aux.dir;
				}
				if (aux == null) {
					return false;
				}
			}
		}
		return true;
	}

	public static boolean contemCpf(Long cpf) {
		if (TreeByCpf.raizCpf == null) {
			return false;
		} else {
			No aux = TreeByCpf.raizCpf;
			while (Funcionario.todos[aux.indice].cpf != cpf.longValue()) {
				if (cpf < Funcionario.todos[aux.indice].cpf) {
					aux = aux.esq;
				} else {
					aux = aux.dir;
				}
				if (aux == null) {
					return false;
				}
			}
		}
		return true;
	}
}
