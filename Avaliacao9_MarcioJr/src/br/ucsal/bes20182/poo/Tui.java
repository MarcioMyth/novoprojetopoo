package br.ucsal.bes20182.poo;

public class Tui {
	public static void main(String[] args) {

		Caminhao c1 = new Caminhao("BOA023", "Reboque", 2015, 10000, 1, 100);
		Caminhao c2 = new Caminhao("OOA893", "Outro", 2002, 45000, 3, 20);
		Caminhao c3 = new Caminhao("JPK987", "Carga", 2014, 95000, 2, 30);
		Caminhao c4 = new Caminhao("JUN160", "Carga", 2001, 0, 1, 10);
		Onibus o1 = new Onibus("ABC456", "Executivo", 2010, 96000, 45);
		Onibus o2 = new Onibus("ABC981", "Turismo", 2010, 50000, 90);
		Onibus o3 = new Onibus("ABC321", "Convencional", 2010, 82000, 50);
		Onibus o4 = new Onibus("JUK321", "Convencional", 2003, -1000, 30);

		Locadora.cadastrarCaminhao(c1);
		Locadora.cadastrarCaminhao(c2);
		Locadora.cadastrarCaminhao(c3);

		Locadora.cadastrarOnibus(o1);
		Locadora.cadastrarOnibus(o2);
		Locadora.cadastrarOnibus(o3);

		System.out.println("Por Placa:");
		Locadora.listarPorPlaca();
		System.out.println();
		System.out.println("Por Valor: ");
		Locadora.listarPorValor();
		System.out.println();
		System.out.println("Modelos: ");
		Locadora.modelos();

		Locadora.cadastrarCaminhao(c4);
		Locadora.cadastrarOnibus(o4);
	}
}
