package br.ucsal.bes20182.poo;

public class ValorNaoValidoException extends Exception {

	private static final long serialVersionUID = 1L;

	public ValorNaoValidoException(String mensagem) {
		super(mensagem);
		
	}
}
