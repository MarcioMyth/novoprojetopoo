package br.ucsal.bes20182.poo;

public abstract class Veiculo {
	private String placa;
	private String modelo;
	private int anoFab;
	private double valor;

	public Veiculo(String placa, String modelo, int anoFab, double valor) {
		this.placa = placa;
		this.modelo = modelo;
		this.anoFab = anoFab;
		this.valor = valor;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getAnoFab() {
		return anoFab;
	}

	public void setAnoFab(int anoFab) {
		this.anoFab = anoFab;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public static void valorException(double valor) throws ValorNaoValidoException {
		if (valor <= 0) {
			throw new ValorNaoValidoException("Imposs�vel adicionar ve�culo, Valor inv�lido!");
		}
	}

	@Override
	public String toString() {
		return "Veiculo [placa=" + placa + ", modelo=" + modelo + ", anoFab=" + anoFab + ", valor=" + valor + "]";
	}

}
