package br.ucsal.bes20182.poo;

import java.util.Comparator;

public class OrdenarPorValor implements Comparator<Veiculo> {

	public int compare(Veiculo veiculo1, Veiculo veiculo2) {
		if (veiculo1.getValor() > veiculo2.getValor()) {
			return 1;
		} else if (veiculo1.getValor() < veiculo2.getValor()) {
			return -1;
		}
		return 0;
	}

}
